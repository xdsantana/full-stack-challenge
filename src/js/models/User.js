const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  user_id: Number,
  first_name: String,
  last_name: String,
  is_shown: Boolean,
});

module.exports = mongoose.model('User', UserSchema);
