const mongoose = require('mongoose');

const MessageSchema = new mongoose.Schema({
  text: String,
  user_id: Number,
  date: Number,
});

module.exports = mongoose.model('MessageSchema', MessageSchema);
