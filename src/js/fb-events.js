const $ = require('jquery');
const TweenMax = require('gsap');

$('#fb-login-btn').click(() => {
  FB.login((response) => {
    if (response.authResponse) {
      // User is logged in
      setTimeout(() => {
        $('.login-container, #login-container-fluid').fadeOut('slow', () => {
          $('#messages-container-fluid').fadeIn('slow');
          TweenMax.to($('body'), 1, { backgroundColor: '#363151' });
        });
      }, 100);
    } else {
      // User did not log in
    }
  });
});

$('#logout').click(() => {
  FB.logout((response) => {
    if (response.authResponse) {
      // User is logged out in
      setTimeout(() => {
        $('.login-container, #login-container-fluid').fadeOut('slow', () => {
          $('#messages-container-fluid').fadeIn('slow');
          TweenMax.to($('body'), 1, { backgroundColor: '#363151' });
        });
      }, 100);
      setTimeout(() => {
        TweenMax.to($('body'), 1, { backgroundColor: '#5f4b8b' });
        $('#messages-container-fluid').fadeOut('slow', () => {
          $('.login-container, #login-container-fluid').fadeIn('slow');
        });
      }, 100);
    } else {
      // User did not logout
    }
  });
});
