/* eslint class-methods-use-this: ["error", { "exceptMethods": ["handle"] }] */
const Telegram = require('telegram-node-bot');

class OtherwiseController extends Telegram.TelegramBaseController {
  handle($) {
    $.sendMessage('I don\'t know this command. Sorry...');
  }
}

module.exports = OtherwiseController;
