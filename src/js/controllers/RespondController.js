/* eslint class-methods-use-this: 0 */
/* eslint no-underscore-dangle: 0 */
const Telegram = require('telegram-node-bot');
const Server = require('../server');

class RespondController extends Telegram.TelegramBaseController {
  RespondHandler($) {
    Server.saveMessage($);
    $.sendMessage(`Welcome, ${$.message._chat._firstName}`);
  }

  get routes() {
    return {
      respondCommand: 'RespondHandler',
    };
  }
}

module.exports = RespondController;
