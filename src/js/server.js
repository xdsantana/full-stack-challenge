/* eslint no-underscore-dangle: 0 */
const Telegram = require('telegram-node-bot');
const mongoose = require('mongoose');
const Message = require('./models/Message');

mongoose.Promise = global.Promise;

mongoose.connect('mongodb://root:passroot@ds247078.mlab.com:47078/dba')
  .then(() => console.log('connection succesful'))
  .catch(err => console.error(err));

const tg = new Telegram.Telegram('514793455:AAHVUsu4dtakFHEfsMKfpVO7ZEbQf0_-7UI', {
  workers: 1,
});

const RespondController = require('./controllers/RespondController');
const OtherwiseController = require('./controllers/otherwise');

tg.router.when(new Telegram.TextCommand('/start', 'respondCommand'), new RespondController())
  .otherwise(new OtherwiseController());

exports.saveMessage = function saveMessage(obj) {
  const user = new Message({
    text: obj.message._text,
    user_id: obj.message._chat._id,
    date: obj.message._date,
  });

  user.save((err, data) => {
    if (err) console.log(err);
    else console.log('Saved : ', data);
  });
};
