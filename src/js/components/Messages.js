/* eslint max-len: ["error", { "code": 200 }] */
import React from 'react';

export default class Messages extends React.Component {
  render() {
    return (
      <div id="messages-container-fluid" className="container-fluid">
        <div id="fb-logout">
          <span id="logout">logout</span>
        </div>
        <div className="messages-container" />
        <form className="form-inline">
          <input id="message-input" type="text" placeholder="Write a Message..." disabled="disabled" />
          <button id="send-message" disabled>send</button>
        </form>
      </div>
    );
  }
}
