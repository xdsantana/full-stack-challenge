/* eslint max-len: ["error", { "code": 200 }] */
import React from 'react';

export default class Login extends React.Component {
  render() {
    return (
      <div id="login-container-fluid" className="container-fluid">
        <div className="login-container">
          <div id="circle-icon-login" />
          <div id="fb-login-btn" />
        </div>
      </div>
    );
  }
}
