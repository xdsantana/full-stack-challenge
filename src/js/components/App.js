import React from 'react';

import Messages from './Messages';
import Login from './Login';

export default class App extends React.Component {
  render() {
    return (
      <div className="container">
        <Login />
        <Messages />
      </div>
    );
  }
}
