# Full Stack Challenge

The main goal of this application was to develop a RESTful API to exchange messages between a Telegram User and a Telegram Bot in real time through a panel, accessed by Facebook-account, where the administrator could login and write responses as if he/she were the real robot behind the bot.

## First steps
```
git clone git@gitlab.com:xdsantana/full-stack-challenge.git
```
or
```
git clone https://gitlab.com/xdsantana/full-stack-challenge.git
```

### Build
```
sudo npm run start:dev
```

Now go to the browser and open ...http://localhost:9000... to see the web application.

### Start Server

on a new Terminal window, go to the project's folder and start the server using the command line below:

```
sudo npm run server
```

### Send Message to Boot

On Telegram, serch for ...Full Stack Challenge Boot... and start a conversation with the command:
```
/start
```
