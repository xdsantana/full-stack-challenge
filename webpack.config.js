const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const webpack = require('webpack');

module.exports = {

  entry: ['./src/js/index.js',
    './src/js/main.js',
    './src/js/libs/facebook-sdk.js',
    './src/js/fb-events.js',
    './src/stylesheets/style.scss'],

  output: {
    filename: './dist/js/script.bundle.js',
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['latest', 'react'],
          },
        },
      },
      {
        test: /\.(css|sass|scss)$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [{
            loader: 'css-loader',
            options: {
              minimize: false,
            },
          }, {
            loader: 'postcss-loader',
          }, {
            loader: 'sass-loader',
          }],
        }),
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
            },
          },
        ],
      },
    ],
  },

  devServer: {
    contentBase: path.join(__dirname, '/'),
    compress: true,
    port: 9000,
  },
  resolve: {
    modules: [
      path.join(__dirname, 'js/helpers'),
      'node_modules',
    ],
    alias: {
      images$: path.resolve(__dirname, './src/img'),
    },
  },
  plugins: [
    new ExtractTextPlugin({
      filename: './dist/stylesheets/[name].bundle.css',
      allChunks: true,
    }),
    // new UglifyJsPlugin({ parallel: true }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
    }),
  ],

};
